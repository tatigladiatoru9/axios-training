const express = require('express');
const app = express();
const { Sequelize, DataTypes } = require('sequelize');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const sequelize = new Sequelize('axiosdb', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    define: {
        timestamps: true
    }
})

const User = sequelize.define('User', {
    name: DataTypes.STRING,
    surname: DataTypes.STRING,
    age: DataTypes.INTEGER,
    email: DataTypes.STRING,
})

const Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    price: DataTypes.FLOAT,
    quantity: DataTypes.INTEGER,
    customId: DataTypes.STRING
})

app.get('/reset', async (req, res) => {
    try {
        await sequelize.sync({ force: true })
        return res.status(200).send({ message: "Reset db successfully" })
    } catch (err) {
        return res.status(500).send(err);
    }
})

app.post('/addUser', async (req, res) => {
    const user = {
        name: req.body.name,
        surname: req.body.surname,
        age: req.body.age,
        email: req.body.email
    }

    let errors = {};

    if (!user.name) {
        errors.name = "Empty name";
    }
    if (!user.surname) {
        errors.surname = "Empty surname";
    }
    if (!user.age) {
        errors.age = "Empty age";
    } else if (isNaN(user.age)) {
        errors.invalidAge = "Invalid age format should be number only";
    }

    if (Object.keys(errors).length === 0) {
        User.create(user).then(() => res.status(201).send({ message: "Created" }))
            .catch(() => res.status(500).send({ message: "Some server error occured" }))
    } else {
        return res.status(400).send(errors);
    }
})

app.post('/addProduct', async (req, res) => {
    const product = {
        name: req.body.name,
        price: req.body.price,
        quantity: req.body.quantity,
        customId: req.body.customId
    }

    let errors = {};
    if (!product.name) {
        errors.prodName = "Product name empty";
    }

    if (isNaN(product.price) || isNaN(product.quantity)) {
        errors.invalidInput = "Input on price or quantity is invalid"
    }

    if (!product.customId) {
        errors.customId = "Product custom id is empty";
    }

    if (Object.keys(errors).length === 0) {
        Product.create(product).then(() => res.status(201).send({ message: "Product created" }))
            .catch(() => res.status(500).send({ message: "Some server error occured" }))
    } else {
        return res.status(400).send(errors);
    }
})

app.put('/updateInfo/:userId', async (req, res) => {
    const user = await User.findByPk(req.params.userId);
    if (user) {
        user.update({
            name: req.body.name,
            surname: req.body.surname,
            age: req.body.age,
            email: req.body.email
        }).then(() => res.status(200).send({ message: "User updated" }))
            .catch(() => res.status(500).send({ message: "Some server error occured" }))
    } else {
        return res.status(400).send({ message: "There is not any user with this id." })
    }
})

app.get('/getAllProducts', async (req, res) => {
    Product.findAll().then((products) => res.status(200).send(products))
        .catch(() => res.status(500).send({ message: "Server error" }))
})

app.get('/getProduct/:customId', async (req, res) => {
    try {
        const product = await Product.findByPk(req.params.customId);
        if (product) {
            return res.status(200).send(product);
        } else {
            return res.status(404).send({ message: "Not found" })
        }
    } catch (err) {
        return res.status(500).send(err);
    }
})

app.delete('/removeProduct/:customId', async (req, res) => {
    const product = await Product.findOne({ where: { customId: req.params.customId } });
    if (product) {
        await product.destroy();
        return res.status(200).send({ message: "Product deleted" })
    } else {
        return res.status(404).send({ message: "Not found" })
    }
})

// const cors = require('cors')
// const corsOptions = {
//     origin: true,
//     allowedHeaders: [
//         "Content-Type",
//         "Authorization",
//         "Access-Control-Allow-Methods",
//         "Access-Control-Request-Headers",
//     ],
//     credentials: true,
//     enablePreflight: true,
// };
// app.use(cors(corsOptions));
// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "http://localhost:8080");
//     res.header(
//         "Access-Control-Allow-Headers",
//         "Origin, X-Requested-With, Content-Type, Accept"
//     );
//     res.header("Access-Control-Allow-Methods", "*");
//     res.header("Access-Control-Allow-Credentials", "true");
//     next();
// });

app.listen(8080, () => {
    console.log('Server is running on 8080')
})

app.use("/", express.static("../front-end"));